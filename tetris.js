const canvas = document.getElementById('tetris');
const context = canvas.getContext('2d');

var widthWindow = document.documentElement.clientHeight;
var body = document.querySelector('body');

body.style.height = widthWindow+"px";

context.scale(30, 30);

const colors = [
    '#FF0D72',
    '#0DC2FF',
    '#0DFF72',
    '#F538FF',
    '#FF8E0D',
    '#FFE138',
    '#3877FF'
];


function Player() {
    this.pos = {x: 0, y: 0};
    this.matrix = null;
    this.score = 0;
    this.lines = 0;
    this.colour = null;
}

Player.prototype.createMatrix = function(w, h) {
const matrix = [];
    while (h--) {
        matrix.push(new Array(w).fill(0));
    }
    return matrix;
}

Player.prototype.setMatrix = function(matrix) {
    this.matrix = matrix;
}

Player.prototype.getMatrix = function() {
    return this.matrix;
}

Player.prototype.setScore = function(score) {
    this.score = score;
}

Player.prototype.getScore = function() {
    return this.score;
}

Player.prototype.playerDrop = function() {
    this.pos.y++;
    if (collide(arena)) {
        this.pos.y--;
        merge(arena);
        playerReset();
        arenaSweep();
        updateScore();
    }
    dropCounter = 0;
}

Player.prototype.playerMove = function(offset) {
    this.pos.x += offset;
    if (collide(arena)) {
        this.pos.x -= offset;
    }
}

Player.prototype.playerRotate = function(dir) {
    const pos = this.pos.x;
    let offset = 1;
    rotate(this.matrix, dir);
    while (collide(arena)) {
        this.pos.x += offset;
        offset = -(offset + (offset > 0 ? 1 : -1));
        if (offset > this.matrix[0].length) {
            rotate(this.matrix, -dir);
            this.pos.x = pos;
            return;
        }
    }
}

Player.prototype.getColour = function() {
    return this.colour;
}

Player.prototype.setColour = function(colour) {
    this.colour = colour;
}



var p = new Player();


function arenaSweep() {
    let rowCount = 1;
    outer: for (let y = arena.length -1; y > 0; --y) {
        for (let x = 0; x < arena[y].length; ++x) {
            if (arena[y][x] === 0) {
                continue outer;
            }
        }

        const row = arena.splice(y, 1)[0].fill(0);
        arena.unshift(row);
        ++y;

        p.score += rowCount * 10;
        p.lines += rowCount;
        rowCount *= 2;
    }
}

function collide(arena) {
    const m = p.getMatrix();
    const o = p.pos;
    for (let y = 0; y < m.length; ++y) {
        for (let x = 0; x < m[y].length; ++x) {
            if (m[y][x] !== 0 &&
               (arena[y + o.y] &&
                arena[y + o.y][x + o.x]) !== 0) {
                return true;
            }
        }
    }
    return false;
}

function createPiece(type)
{
    if (type === 'I') {
        return [
            [0, 1, 0, 0],
            [0, 1, 0, 0],
            [0, 1, 0, 0],
            [0, 1, 0, 0],
        ];
    } else if (type === 'L') {
        return [
            [0, 2, 0],
            [0, 2, 0],
            [0, 2, 2],
        ];
    } else if (type === 'J') {
        return [
            [0, 3, 0],
            [0, 3, 0],
            [3, 3, 0],
        ];
    } else if (type === 'O') {
        return [
            [4, 4],
            [4, 4],
        ];
    } else if (type === 'Z') {
        return [
            [5, 5, 0],
            [0, 5, 5],
            [0, 0, 0],
        ];
    } else if (type === 'S') {
        return [
            [0, 6, 6],
            [6, 6, 0],
            [0, 0, 0],
        ];
    } else if (type === 'T') {
        return [
            [0, 7, 0],
            [7, 7, 7],
            [0, 0, 0],
        ];
    }
}

var rand = Math.floor(Math.random() * colors.length);
p.setColour(colors[rand]);

function drawMatrix(matrix, offset) {
                
    matrix.forEach((row, y) => {
        row.forEach((value, x) => {
            if (value !== 0) {
                var color = p.getColour();
                console.log(color);
                context.fillStyle = color;
                context.fillRect(x + offset.x,
                                 y + offset.y,
                                 1, 1);
            }
        });
    });
}

function draw() {
    context.fillStyle = "rgb(0, 0, 0)";
context.fillRect(0, 0, canvas.width, canvas.height);

    drawMatrix(arena, {x: 0, y: 0});
    drawMatrix(p.getMatrix(), p.pos);
}

function merge(arena) {
    var k = p.getMatrix();
    k.forEach((row, y) => {
        row.forEach((value, x) => {
            if (value !== 0) {
                arena[y + p.pos.y][x + p.pos.x] = value;
            }
        });
    });
}

function rotate(matrix, dir) {
    for (let y = 0; y < matrix.length; ++y) {
        for (let x = 0; x < y; ++x) {
            [
                matrix[x][y],
                matrix[y][x],
            ] = [
                matrix[y][x],
                matrix[x][y],
            ];
        }
    }

    if (dir > 0) {
        matrix.forEach(row => row.reverse());
    } else {
        matrix.reverse();
    }
}



function playerReset() {
    const pieces = 'TJLOSZI';
    var piece = createPiece(pieces[pieces.length * Math.random() | 0]);
    p.setMatrix(piece);
    p.pos.y = 0;
    var matr = p.getMatrix();
    p.pos.x = (arena[0].length / 2 | 0) -
                   (matr[0].length / 2 | 0);
    if (collide(arena)) {
        alert("Game over!!!");
        arena.forEach(row => row.fill(0));
        p.score = 0;
        updateScore();
    }
}


var lastTime = 0;
var dropCounter = 0;
var dropInterval = 1000;

function update(time = 0) {
    const deltaTime = time - lastTime;

    dropCounter += deltaTime;
    if (dropCounter > dropInterval) {
        p.playerDrop();
    }

    lastTime = time;

    draw();
    requestAnimationFrame(update);
}

function updateScore() {
    document.querySelector('.score').innerText = p.score;
    document.querySelector('.lines').innerText = p.lines;
}

document.addEventListener('keydown', event => {
    if (event.keyCode === 37) {
        p.playerMove(-1);
    } else if (event.keyCode === 39) {
        p.playerMove(1);
    } else if (event.keyCode === 40) {
        p.playerDrop();
    } else if (event.keyCode === 38) {
        p.playerRotate(1);
    }
});



const arena = p.createMatrix(10, 20);



playerReset();
updateScore();
update();


const arr = [
'#FF5733',
'#58FF33',
'#33FFF9',
'#3361FF',
'#FF33DD',
'#FF3333'

]






// window.onkeydown = function(e) {
// if(e.keyCode ===  87) {
//     var rand = Math.floor(Math.random() * arr.length);
//     p.setColour(arr[rand]);
// console.log(arr[rand]);
// }
// else if(e.keyCode ===  83) {
//     var k = document.querySelector('#game');
//   if(k.classList.contains('rotate0')) {
//     k.classList.remove('rotate0');
//     k.classList.add('rotate90');
//      }
//   else if(k.classList.contains('rotate90')) {
//     k.classList.remove('rotate90');
//     k.classList.add('rotate180');
//   }
//   else if(k.classList.contains('rotate180')){
//     k.classList.remove('rotate180');
//         k.classList.add('rotate270');
//   }
//   else if(k.classList.contains('rotate270')) {
//     k.classList.remove('rotate270');
//     k.classList.add('rotate360');
//   }
//   else if(k.classList.contains('rotate360')) {
//     k.classList.remove('rotate360');
//     k.classList.add('rotate0');
//   }

// }

// }